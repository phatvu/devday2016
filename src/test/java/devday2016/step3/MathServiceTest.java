package devday2016.step3;

import devday2016.mocktest.step3.Calculator;
import devday2016.mocktest.step3.MathService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class MathServiceTest {
    @Mock
    Calculator calculator;

    @InjectMocks
    MathService mathService;

    @Test
    public void testAdd() {
        //given
        given(calculator.add(1, 2)).willReturn(3);

        // when
        int actualResult = mathService.add(1, 2);

        // then
        assertThat("add 2 positive numbers", actualResult, equalTo(3));
    }

}
