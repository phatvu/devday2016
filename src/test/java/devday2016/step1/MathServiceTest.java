package devday2016.step1;

import devday2016.mocktest.step1.MathService;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class MathServiceTest {
    MathService mathService;

    @Test
    public void testAdd() {
        mathService = new MathService();
        assertThat("add 2 positive numbers", mathService.add(1, 2), equalTo(3));
    }

}
