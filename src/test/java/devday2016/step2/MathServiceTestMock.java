package devday2016.step2;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import devday2016.mocktest.step2.Calculator;
import devday2016.mocktest.step2.MathService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(DataProviderRunner.class)
public class MathServiceTestMock {
    @InjectMocks
    MathService mathService;

    @Mock
    Calculator calculator;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testAdd() {
        //given
        given(calculator.add(1, 2)).willReturn(3);

        // when
        int actualResult = mathService.add(1, 2);

        // then
        assertThat("add 2 positive numbers", actualResult, equalTo(3));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivide_Exception_Expected() {
        //given
        doThrow(new ArithmeticException()).when(calculator).divide(anyInt(), eq(0));

        // when
        mathService.divide(9, 0);
    }

    @Test
    public void demoSpy() {
        Calculator calculator = new CalculatorImpl();
        Calculator spy = spy(calculator);

        // stub out some methods
        given(spy.divide(9, 2)).willReturn(100d);

        //  using the spy calls real methods
        int realResult = spy.add(2, 5);
        // divide() method was stubbed - 100d is result
        double stubResult = spy.divide(9, 2);

        assertThat("test real call", realResult, equalTo(7));
        assertThat("test stubbed call", stubResult, equalTo(100d));
    }


    @DataProvider
    public static Object[][] testDataCollection() {
        return new Object[][]{
                {1  ,  2,  3},
                {-1 ,  3,  2},
                {-2 , -3, -5},
        };
    }

    @Test
    @UseDataProvider("testDataCollection")
    public void testAdd_Dataprovider(int num1, int num2, int expected) {
        given(calculator.add(num1, num2)).willReturn(expected);
        int actualResult = mathService.add(num1, num2);
        assertThat(actualResult, equalTo(expected));
    }

}
