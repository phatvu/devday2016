package devday2016.step2;

import devday2016.mocktest.step2.Calculator;
import devday2016.mocktest.step2.MathService;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MathServiceTest {
    MathService mathService;

    @Test
    public void testAdd() {
        Calculator calculator = new CalculatorImpl();
        mathService = new MathService(calculator);
        assertThat("add 2 positive numbers", mathService.add(1, 2), equalTo(3));
    }
}
