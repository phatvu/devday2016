package devday2016.step2;

import devday2016.mocktest.step2.Calculator;

public class CalculatorImpl implements Calculator {
    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public double divide(int a, int b) {
        return 0;
    }
}
