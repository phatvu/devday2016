package devday2016.mocktest.step3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MathService {
    @Autowired
    private Calculator calculator;

    public int add(int a, int b) {
        int result = calculator.add(a, b);
        return result;
    }
}
