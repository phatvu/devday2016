package devday2016.mocktest.step2;

public class MathService {
    private Calculator calculator;

    public MathService(Calculator calculator) {
        this.calculator = calculator;
    }

    public int add(int a, int b) {
        int result = calculator.add(a, b);
        return result;
    }

    public double divide (int a, int b) {
        double result = calculator.divide(a, b);
        return result;
    }

}