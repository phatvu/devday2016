package devday2016.mocktest.step2;

public interface Calculator {
    int add(int a, int b);
    double divide(int a, int b);
}